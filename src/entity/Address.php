<?php
/*
 * @copyright 2019-2022 Dicr http://dicr.org
 * @author Igor A Tarasov <develop@dicr.org>
 * @license BSD-3-Clause
 * @version 04.01.22 19:10:19
 */

declare(strict_types = 1);
namespace dicr\esputnik\entity;

use dicr\esputnik\Entity;

/**
 * Полный адрес контакта.
 *
 * @link https://esputnik.com/api/ns0_address.html
 */
class Address extends Entity
{
    /** @var ?string Область */
    public ?string $region = null;

    /** @var ?string Город */
    public ?string $town = null;

    /** @var ?string Адрес */
    public ?string $address = null;

    /** @var string|int|null Почтовый индекс. */
    public string|int|null $postcode = null;

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            ['postcode', 'number', 'min' => 1, 'max' => 999999]
        ]);
    }
}
