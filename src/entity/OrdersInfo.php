<?php
/*
 * @copyright 2019-2022 Dicr http://dicr.org
 * @author Igor A Tarasov <develop@dicr.org>
 * @license BSD-3-Clause
 * @version 04.01.22 19:22:34
 */

declare(strict_types = 1);
namespace dicr\esputnik\entity;

use dicr\esputnik\Entity;

use function array_merge;

/**
 * Информация о заказах.
 *
 * @link https://esputnik.com/api/ns1_ordersInfo.html
 */
class OrdersInfo extends Entity
{
    /** @var string|null Дата последнего заказа в формате "YYYY-MM-DD\THH:mm:ss. */
    public ?string $lastDate = null;

    /** @var int|null Общее количество заказов. */
    public ?int $count = null;

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            ['lastDate', 'date', 'format' => 'php:Y-m-d\TH:i:s'],

            ['count', 'integer', 'min' => 0],
            ['count', 'filter', 'filter' => 'intval']
        ]);
    }
}
