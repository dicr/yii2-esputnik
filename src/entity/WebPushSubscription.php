<?php
/*
 * @copyright 2019-2022 Dicr http://dicr.org
 * @author Igor A Tarasov <develop@dicr.org>
 * @license BSD-3-Clause
 * @version 04.01.22 19:25:01
 */

declare(strict_types = 1);
namespace dicr\esputnik\entity;

use dicr\esputnik\Entity;

/**
 * Подписка Web-push.
 *
 * @link https://esputnik.com/api/ns0_webPushSubscription.html
 */
class WebPushSubscription extends Entity
{
    /** @var int|null Id приложения подписки (int) в eSputnik */
    public ?int $appId = null;

    /** @var string|null Идентификатор браузера */
    public ?string $userAgent = null;

    /** @var string|null Версия браузера */
    public ?string $userAgentVersion = null;

    /** @var string|null Язык */
    public ?string $userAgentLanguage = null;

    /** @var string|null Операционная система */
    public ?string $os = null;

    /** @var string|null IP пользователя */
    public ?string $ip = null;

    /** @var string|null Страница подписки */
    public ?string $subscriptionPage = null;

    public ?string $swActiveVersion = null;

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            ['appId', 'integer', 'min' => 1],
            ['appId', 'filter', 'filter' => 'intval', 'skipOnEmpty' => true]
        ]);
    }
}
