<?php
/*
 * @copyright 2019-2022 Dicr http://dicr.org
 * @author Igor A Tarasov <develop@dicr.org>
 * @license BSD-3-Clause
 * @version 04.01.22 19:13:51
 */

declare(strict_types = 1);

namespace dicr\esputnik\entity;

use dicr\esputnik\Entity;
use dicr\json\EntityValidator;

use function array_merge;

/**
 * Контакт.
 *
 * @link https://esputnik.com/api/ns0_contact.html
 */
class Contact extends Entity
{
    /** @var ?string Имя контакта. Цифры использовать нельзя. */
    public ?string $firstName = null;

    /** @var ?string Фамилия контакта. Цифры использовать нельзя. */
    public ?string $lastName = null;

    /** @var Channel[] Обязательный параметр. Список медиа-каналов контакта. */
    public array $channels;

    /** @var ?Address Адрес контакта. */
    public ?Address $address = null;

    /** @var ContactField[]|null Значения дополнительных полей контакта. */
    public ?array $fields = null;

    /**
     * @var ?int Идентификатор каталога.
     * Информацию о каталогах можно получить с помощью метода /v1/addressbooks GET.
     */
    public ?int $addressBookId = null;

    /** @var ?int Идентификатор контакта. */
    public ?int $id = null;

    /** @var ?string Ключ контакта. */
    public ?string $contactKey = null;

    /** @var ?OrdersInfo Информация о заказах. */
    public ?OrdersInfo $ordersInfo = null;

    /**
     * @var GroupDTO[]|null Список СТАТИЧЕСКИХ групп, в которые входит контакт.
     * Используется только при получении контактов.
     */
    public ?array $groups = null;

    /** @var ?string Язык контакта. */
    public ?string $languageCode = null;

    /** @var ?string Временная зона контакта. */
    public ?string $timeZone = null;

    /**
     * @inheritDoc
     */
    public function attributeEntities(): array
    {
        return array_merge(parent::attributeEntities(), [
            'channels' => [Channel::class],
            'address' => Address::class,
            'fields' => [ContactField::class],
            'ordersInfo' => OrdersInfo::class,
            'groups' => [GroupDTO::class]
        ]);
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            [['firstName', 'lastName'], 'trim'],
            [['firstName', 'lastName'], 'default'],

            [['channels', 'address', 'fields', 'ordersInfo', 'groups'], EntityValidator::class],

            [['addressBookId', 'id'], 'integer', 'min' => 1],
            [['addressBookId', 'id'], 'filter', 'filter' => 'intval', 'skipOnEmpty' => true],
        ]);
    }
}
